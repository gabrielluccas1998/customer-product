﻿using CustomerProduct.App.Data.ModelsViewModels;
using Microsoft.AspNetCore.Components;

namespace CustomerProduct.App.Areas.Identity.Pages.ProductPages
{
    public partial class ProductEdit
    {
        [Parameter]
        public string? ProductID { get; set; }
        private ProductViewModel ProductVM = new ProductViewModel();
        protected async Task OnValidSubmission()
        {
            if (ProductID == null)
            {
                ProductVM.CreatedBy = httpContextAcessor.HttpContext.User.Identity.Name;
            }
            else
            {
                ProductVM.ModifiedBy = httpContextAcessor.HttpContext.User.Identity.Name;
            }
            await uow.ProductRepo.CreateOrUpdateProductAsync(ProductVM);
            NavManager.NavigateTo("/productsoverview");
        }

        protected async override Task OnInitializedAsync()
        {
            if (ProductID != null) 
            {
                var product = await uow.ProductRepo.GetProductByIdAsync(int.Parse(ProductID));
                if (product != null) ProductVM = new ProductViewModel { ProductID = product.ProductID, Title = product.Title };
            }
        }
    }
}
