﻿using CustomerProduct.App.Component;
using CustomerProduct.App.Data.Models;
using CustomerProduct.App.Data.ModelsViewModels;
using ProductsServerApp.Data;

namespace CustomerProduct.App.Areas.Identity.Pages.ProductPages
{
    public partial class ProductOverview
    {
        private List<ProductViewModel> products;
        protected AddProductDialog AddProductDialog { get; set; }

        protected override async Task OnInitializedAsync()
        {
            products = await uow.ProductRepo.GetProductVMsAsync();
        }

        private void AddProduct()
        {
            AddProductDialog.Show();
        }

        public async void AddProductDialog_OnDialogClose()
        {
            products = await uow.ProductRepo.GetProductVMsAsync();
            StateHasChanged();
        }
    }
}
