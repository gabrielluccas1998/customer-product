﻿using CustomerProduct.App.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CustomerProduct.App.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerState> CustomerStates { get; set; }
        public DbSet<CustomerType> CustomerTypes { get; set; }
        public DbSet<PriceList> PriceLists { get; set; }
        public DbSet<PriceListPosition> PriceListPositions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<PriceListPosition>().HasKey(x => new { x.PriceListID, x.PositionNr });
            builder.Entity<Product>().HasData(
                new Product { ProductID = 1, IsActive = true, Created = DateTime.Now, Title = "AMD Ryzen 7 5800X Box Processor" },
                new Product { ProductID = 2, IsActive = true, Created = DateTime.Now, Title = "Intel Core i9-11900K Desktop Processor" },
                new Product { ProductID = 3, IsActive = true, Created = DateTime.Now, Title = "Amd Ryzen 5 2600X Processor" }
            );
            builder.Entity<CustomerState>().HasData(
                new CustomerState { CustomerStateID = 1, Title = "Active", Notes = "When the customer is active buying" },
                new CustomerState { CustomerStateID = 2, Title = "Restricted", Notes = "This customer does suffer from some restriction" },
                new CustomerState { CustomerStateID = 3, Title = "Inactive", Notes = "When the customer has not made a purchase for a long period of time" }
            );
            builder.Entity<CustomerType>().HasData(
                new CustomerType { CustomerTypeID = 1, Title = "New Customer", Notes = "A customer who started now" },
                new CustomerType { CustomerTypeID = 2, Title = "Loyal Customer", Notes = "Customer that is actively purchasing for a long period of time" },
                new CustomerType { CustomerTypeID = 3, Title = "Normal Customer", Notes = "A customer who buys regularly for a short period of time" }
            );
            builder.Entity<Currency>().HasData(
                new Currency { CurrencyID = 1, Title = "Euro", Symbol = "€" },
                new Currency { CurrencyID = 2, Title = "Dollar", Symbol = "$" },
                new Currency { CurrencyID = 3, Title = "Brazilian Real", Symbol = "R$" }
            );
            builder.Entity<PriceList>().HasData(
                new PriceList { PriceListID = 1, Title = "Black Friday", CurrencyID = 1, ValidFrom = new DateTime(2022, 11, 25), ValidTo = new DateTime(2022, 11, 25) },
                new PriceList { PriceListID = 2, Title = "Easter Action", CurrencyID = 1, ValidFrom = new DateTime(2022, 4, 11), ValidTo = new DateTime(2022, 4, 17) },
                new PriceList { PriceListID = 3, Title = "Summer Action", CurrencyID = 1, ValidFrom = new DateTime(2022, 5, 22), ValidTo = new DateTime(2022, 6, 30) }
            );
            builder.Entity<PriceListPosition>().HasData(
                new PriceListPosition { PriceListID = 1, PositionNr = 1, ProductID = 1, Price = 420 },
                new PriceListPosition { PriceListID = 1, PositionNr = 2, ProductID = 2, Price = 450 },
                new PriceListPosition { PriceListID = 1, PositionNr = 3, ProductID = 3, Price = 210 }
            );
        }
    }
}