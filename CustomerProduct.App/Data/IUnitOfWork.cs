﻿using CustomerProduct.App.Data.Repositories;

namespace ProductsServerApp.Data
{
    public interface IUnitOfWork
    {
        ProductRepo ProductRepo { get; }

        Task CommitAsync();
    }
}