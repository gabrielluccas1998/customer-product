﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CustomerProduct.App.Data.Migrations
{
    public partial class SampleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "CustomerStates",
                columns: new[] { "CustomerStateID", "Notes", "Title" },
                values: new object[,]
                {
                    { 1, "When the customer is active buying", "Active" },
                    { 2, "This customer does suffer from some restriction", "Restricted" },
                    { 3, "When the customer has not made a purchase for a long period of time", "Inactive" }
                });

            migrationBuilder.InsertData(
                table: "CustomerTypes",
                columns: new[] { "CustomerTypeID", "Notes", "Title" },
                values: new object[,]
                {
                    { 1, "A customer who started now", "New Customer" },
                    { 2, "Customer that is actively purchasing for a long period of time", "Loyal Customer" },
                    { 3, "A customer who buys regularly for a short period of time", "Normal Customer" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ProductID", "Created", "CreatedBy", "IsActive", "Modified", "ModifiedBy", "Title" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9213), null, true, new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9209), null, "AMD Ryzen 7 5800X Box Processor" },
                    { 2, new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9222), null, true, new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9219), null, "Intel Core i9-11900K Desktop Processor" },
                    { 3, new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9230), null, true, new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9227), null, "Amd Ryzen 5 2600X Processor" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerStates",
                keyColumn: "CustomerStateID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CustomerStates",
                keyColumn: "CustomerStateID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CustomerStates",
                keyColumn: "CustomerStateID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CustomerTypes",
                keyColumn: "CustomerTypeID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CustomerTypes",
                keyColumn: "CustomerTypeID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CustomerTypes",
                keyColumn: "CustomerTypeID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 3);
        }
    }
}
