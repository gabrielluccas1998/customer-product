﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CustomerProduct.App.Data.Migrations
{
    public partial class UpdatedSampleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Currencies",
                columns: new[] { "CurrencyID", "Symbol", "Title" },
                values: new object[,]
                {
                    { 1, "€", "Euro" },
                    { 2, "$", "Dollar" },
                    { 3, "R$", "Brazilian Real" }
                });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 1,
                columns: new[] { "Created", "Modified" },
                values: new object[] { new DateTime(2022, 1, 10, 15, 11, 52, 899, DateTimeKind.Local).AddTicks(1717), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 2,
                columns: new[] { "Created", "Modified" },
                values: new object[] { new DateTime(2022, 1, 10, 15, 11, 52, 899, DateTimeKind.Local).AddTicks(1753), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 3,
                columns: new[] { "Created", "Modified" },
                values: new object[] { new DateTime(2022, 1, 10, 15, 11, 52, 899, DateTimeKind.Local).AddTicks(1755), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "PriceLists",
                columns: new[] { "PriceListID", "CurrencyID", "Title", "ValidFrom", "ValidTo" },
                values: new object[] { 1, 1, "Black Friday", new DateTime(2022, 11, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 11, 25, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "PriceLists",
                columns: new[] { "PriceListID", "CurrencyID", "Title", "ValidFrom", "ValidTo" },
                values: new object[] { 2, 1, "Easter Action", new DateTime(2022, 4, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 4, 17, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "PriceLists",
                columns: new[] { "PriceListID", "CurrencyID", "Title", "ValidFrom", "ValidTo" },
                values: new object[] { 3, 1, "Summer Action", new DateTime(2022, 5, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2022, 6, 30, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "PriceListPositions",
                columns: new[] { "PositionNr", "PriceListID", "Price", "ProductID" },
                values: new object[] { 1, 1, 420m, 1 });

            migrationBuilder.InsertData(
                table: "PriceListPositions",
                columns: new[] { "PositionNr", "PriceListID", "Price", "ProductID" },
                values: new object[] { 2, 1, 450m, 2 });

            migrationBuilder.InsertData(
                table: "PriceListPositions",
                columns: new[] { "PositionNr", "PriceListID", "Price", "ProductID" },
                values: new object[] { 3, 1, 210m, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "CurrencyID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "CurrencyID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PriceListPositions",
                keyColumns: new[] { "PositionNr", "PriceListID" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "PriceListPositions",
                keyColumns: new[] { "PositionNr", "PriceListID" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "PriceListPositions",
                keyColumns: new[] { "PositionNr", "PriceListID" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "PriceLists",
                keyColumn: "PriceListID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PriceLists",
                keyColumn: "PriceListID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PriceLists",
                keyColumn: "PriceListID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Currencies",
                keyColumn: "CurrencyID",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 1,
                columns: new[] { "Created", "Modified" },
                values: new object[] { new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9213), new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9209) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 2,
                columns: new[] { "Created", "Modified" },
                values: new object[] { new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9222), new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9219) });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "ProductID",
                keyValue: 3,
                columns: new[] { "Created", "Modified" },
                values: new object[] { new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9230), new DateTime(2021, 12, 21, 15, 14, 37, 422, DateTimeKind.Local).AddTicks(9227) });
        }
    }
}
