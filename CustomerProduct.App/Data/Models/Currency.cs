﻿using System.ComponentModel.DataAnnotations;

namespace CustomerProduct.App.Data.Models
{
    public class Currency
    {
        [Key]
        public int CurrencyID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required, StringLength(6)]
        public string? Symbol { get; set; }
        public virtual ICollection<Customer>? Customers { get; set; }
        public virtual ICollection<PriceList>? PriceLists{ get; set; }
    }
}
