﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerProduct.App.Data.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        [StringLength(50)]
        public string? FirstName { get; set; }
        [Required, StringLength(50)]
        public string? LastName { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        public double Discount { get; set; }
        public string? Notes { get; set; }
        [Required]
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime Modified { get; set; }
        [ForeignKey("Currency")]
        public int CurrencyID { get; set; }
        [ForeignKey("CustomerType")]
        public int CustomerTypeID { get; set; }
        [ForeignKey("PriceList")]
        public int PriceListID { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }
        public virtual Currency? Currency { get; set; }
        public virtual CustomerType? CustomerType { get; set; }
        public virtual PriceList? PriceList { get; set; }
        public virtual ICollection<CustomerState>? CustomerStates { get; set; }

    }
}
