﻿using System.ComponentModel.DataAnnotations;

namespace CustomerProduct.App.Data.Models
{
    public class CustomerState
    {
        [Key]
        public int CustomerStateID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        public string? Notes { get; set; }
        public virtual ICollection<Customer>? Customers { get; set; }
    }
}
