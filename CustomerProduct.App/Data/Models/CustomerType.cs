﻿using System.ComponentModel.DataAnnotations;

namespace CustomerProduct.App.Data.Models
{
    public class CustomerType
    {
        [Key]
        public int CustomerTypeID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [StringLength(100)]
        public string? Notes { get; set; }
        public virtual ICollection<Customer>? Customers { get; set; }
    }
}
