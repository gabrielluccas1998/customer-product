﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerProduct.App.Data.Models
{
    public class PriceList
    {
        [Key]
        public int PriceListID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required]
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        [Required, ForeignKey("Currency")]
        public int CurrencyID { get; set; }
        public Currency? Currency { get; set; }
        public virtual ICollection<Customer>? Customers { get; set; }
        public virtual ICollection<PriceListPosition>? PriceListPositions { get; set; }
    }
}
