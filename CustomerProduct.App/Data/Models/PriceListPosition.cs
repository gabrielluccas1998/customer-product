﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerProduct.App.Data.Models
{
    public class PriceListPosition
    {
        public int PriceListID { get; set; }
        public int PositionNr { get; set; }
        [Required, ForeignKey("Product")]
        public int ProductID { get; set; }
        [Required, DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public virtual Product? Product { get; set; }
        public virtual PriceList? PriceList { get; set; }
    }
}
