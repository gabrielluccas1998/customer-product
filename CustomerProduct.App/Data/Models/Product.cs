﻿using System.ComponentModel.DataAnnotations;

namespace CustomerProduct.App.Data.Models
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }
        public ICollection<PriceListPosition>? PriceListPositions { get; set; }
    }
}
