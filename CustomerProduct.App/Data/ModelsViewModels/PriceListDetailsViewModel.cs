﻿using CustomerProduct.App.Data.Models;
using Microsoft.Build.Framework;

namespace CustomerProduct.App.Data.ModelsViewModels
{
    public class PriceListDetailsViewModel
    {
        public int PriceListID { get; set; }
        [Required]
        public int PositionNr { get; set; }
        [Required]
        public decimal Price { get; set; }
        public Product? Product { get; set; }
    }
}
