﻿using CustomerProduct.App.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace CustomerProduct.App.Data.ModelsViewModels
{
    public class PriceListViewModel
    {
        [Key]
        public int PriceListID { get; set; }
        [Required, StringLength(50)]
        public string? Title { get; set; }
        [Required]
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public Currency? Currency { get; set; }
    }
}
