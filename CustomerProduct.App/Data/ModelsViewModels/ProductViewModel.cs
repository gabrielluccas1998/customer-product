﻿using System.ComponentModel.DataAnnotations;

namespace CustomerProduct.App.Data.ModelsViewModels
{
    public class ProductViewModel
    {
        [Key]
        public int ProductID { get; set; }
        [Required(AllowEmptyStrings = false), StringLength(50)]
        public string? Title { get; set; }
        [Required]
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }
    }
}
