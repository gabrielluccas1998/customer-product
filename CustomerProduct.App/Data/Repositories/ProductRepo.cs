﻿using CustomerProduct.App.Data.Models;
using CustomerProduct.App.Data.ModelsViewModels;
using Microsoft.EntityFrameworkCore;

namespace CustomerProduct.App.Data.Repositories
{
    public class ProductRepo
    {
        private ApplicationDbContext dbContext;
        public ProductRepo(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await dbContext.Products.FirstOrDefaultAsync(x => x.ProductID == id);
        }
        public async Task<List<Product>> GetProductsAsync()
        {
            return await dbContext.Products.ToListAsync();
        }
        public async Task<List<ProductViewModel>> GetProductVMsAsync()
        {
            var productsVM = from p in dbContext.Products
                             select new ProductViewModel
                             {
                                 ProductID = p.ProductID,
                                 Title = p.Title,
                                 Created = p.Created,
                                 Modified = p.Modified,
                                 CreatedBy = p.CreatedBy,
                                 ModifiedBy = p.ModifiedBy
                             };
            return await productsVM.ToListAsync();
        }

        public async Task<List<PriceListViewModel>> GetPriceListVMsAsync()
        {
            var priceListVM = from p in dbContext.PriceLists
                              select new PriceListViewModel
                              {
                                  PriceListID = p.PriceListID,
                                  Title = p.Title,
                                  ValidFrom = p.ValidFrom,
                                  ValidTo = p.ValidTo,
                                  Currency = p.Currency
                              };
            return await priceListVM.ToListAsync();
        }

        public async Task<List<PriceListDetailsViewModel>> GetPriceListDetailsVMsAsyncByID(int id)
        {
            var priceListDetailsVM = from p in dbContext.PriceListPositions.Where(x => x.PriceListID == id)
                                     select new PriceListDetailsViewModel
                                     {
                                         PriceListID = p.PriceListID,
                                         PositionNr = p.PositionNr,
                                         Price = p.Price,
                                         Product = p.Product
                                     };
            return await priceListDetailsVM.ToListAsync();
        }
        public async Task CreateOrUpdateProductAsync(ProductViewModel productVM)
        {
            var oldProduct = await GetProductByIdAsync(productVM.ProductID);
            if(oldProduct == null)
            {
                var newProduct = new Product { ProductID = productVM.ProductID, Title = productVM.Title, Created = DateTime.Now, CreatedBy = productVM.CreatedBy, IsActive = true };
                dbContext.Products.Add(newProduct);
            }
            else
            {
                oldProduct.Title = productVM.Title;
                oldProduct.Modified = DateTime.Now;
                oldProduct.ModifiedBy = productVM.ModifiedBy;
                dbContext.Products.Update(oldProduct);
            }
            await dbContext.SaveChangesAsync();
        }

        public async Task CreateOrUpdatePriceListAsync(PriceListViewModel priceListVM)
        {
            var oldPriceList = await dbContext.PriceLists.FirstOrDefaultAsync(x => x.PriceListID == priceListVM.PriceListID);
            if (oldPriceList == null)
            {
                var newPriceList = new PriceList { Title = priceListVM.Title, ValidFrom = priceListVM.ValidFrom, ValidTo = priceListVM.ValidTo, CurrencyID = priceListVM.Currency.CurrencyID };
                dbContext.PriceLists.Add(newPriceList);
            }
            else
            {
                oldPriceList.Title = priceListVM.Title;
                oldPriceList.ValidTo = priceListVM.ValidTo;
                oldPriceList.ValidFrom = priceListVM.ValidFrom;
                oldPriceList.CurrencyID = priceListVM.Currency.CurrencyID;
                dbContext.PriceLists.Update(oldPriceList);
            }
            await dbContext.SaveChangesAsync();
        }

        public async Task DeletePriceListAsync(int id)
        {
            var priceList = await dbContext.PriceLists.FirstOrDefaultAsync(x => x.PriceListID==id);
            if (priceList != null)
            {
                dbContext.PriceLists.Remove(priceList);
                await dbContext.SaveChangesAsync();        
            }
        }
    }
}
