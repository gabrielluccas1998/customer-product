﻿using CustomerProduct.App.Data;
using CustomerProduct.App.Data.Repositories;

namespace ProductsServerApp.Data
{
    public class UnitOfWork:IUnitOfWork,IDisposable
    {
        private ApplicationDbContext dbContext;
        private ProductRepo productRepo;

        public UnitOfWork(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public ProductRepo ProductRepo
        {
            get
            {
                if(productRepo == null) productRepo = new ProductRepo(dbContext);
                return productRepo;
            }
        }

        public async Task CommitAsync()
        {
            await dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            dbContext.Dispose();
        }

    }
}
