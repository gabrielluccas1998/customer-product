﻿using CustomerProduct.App.Component;
using CustomerProduct.App.Data.ModelsViewModels;

namespace CustomerProduct.App.Pages
{
    public partial class PriceListOverview
    {
        private List<PriceListViewModel> priceLists { get; set; }
        protected ShowPriceListDetails ShowPriceListDetails { get; set; }

        protected override async Task OnInitializedAsync()
        {
            priceLists = await uow.ProductRepo.GetPriceListVMsAsync();
        }
        private void ShowPriceListDialog(int id)
        {
            ShowPriceListDetails.Show(id);
        }

        public async void ShowPriceListDialog_OnDialogClose()
        {
            priceLists = await uow.ProductRepo.GetPriceListVMsAsync();
            StateHasChanged();
        }
    }
}
